﻿using Refit;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    public interface AddMasterCollo
    {

        [Get("/ws/{cep}/json/")]
        Task<AddMaster> GetAddressAsync( string cep);
    }

    
}
