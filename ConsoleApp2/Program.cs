﻿using System;

using System.Threading.Tasks;
using Refit;

namespace ConsoleApp2
{
    class Program
    {
        static async Task Main(string[] args)
        {
            try
            {
                var cepClient = RestService.For<AddMasterCollo>("http://viacep.com.br");
                Console.WriteLine("digite o cep:");
                string cpinfo  = Console.ReadLine().ToString();
                Console.WriteLine("consultando cep:" + cpinfo);

                
                
                var result = await cepClient.GetAddressAsync(cpinfo);

                Console.Write(result.Logradouro);
                Console.ReadKey();

            }
            catch(Exception e)
            {
                Console.WriteLine("erro:",e.Message);
            }
        }
    }
}
